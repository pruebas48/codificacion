package mx.org.inegi.utility;

/**
 *
 * @author SALVADOR.GUTIERREZ
 */
public final class Utility {

    public static int doInt(String value) {
        int regresa = 0;
        try {
            regresa = Integer.parseInt(value);
        } // try
        catch (NumberFormatException e) {
            regresa = 0;
        } // catch
        return regresa;
    } // getInt

    public static int doInt(Object value) {
        int regresa = 0;
        try {
            if (value != null) {
                regresa = Integer.parseInt(value.toString());
            }
        } // try
        catch (NumberFormatException e) {
            regresa = 0;
        } // catch
        return regresa;
    } // getInt

    public static boolean doBolean(String value) {
        boolean regresa = false;
        try {
            regresa = Boolean.parseBoolean(value);
        } // try
        catch (Exception e) {
            regresa = false;
        } // catch
        return regresa;
    } // getBolean

    public static boolean doBolean(Object value) {
        boolean regresa = false;
        try {
            if (value != null) {
                regresa = Boolean.parseBoolean(value.toString());
            }
        } // try
        catch (Exception e) {
            regresa = false;
        } // catch
        return regresa;
    } // getBolean

    public static String doString(Object value) {
        String regresa = "";
        try {
            if (value != null) {
                regresa = value.toString();
            }
        } // try
        catch (Exception e) {
            regresa = "";
        } // catch
        return regresa;
    } // getString

    public static Double doDouble(String value) {
        Double regresa = 0.0;
        try {
            regresa = Double.valueOf(value);
        } // try
        catch (NumberFormatException e) {
            regresa = 0.0;
        } // catch
        return regresa;
    } // getInt

    public static Double doDouble(Object value) {
        Double regresa = 0.0;
        try {
            if (value != null) {
                regresa = Double.valueOf(value.toString());
            }
        } // try
        catch (NumberFormatException e) {
            regresa = 0.0;
        } // catch
        return regresa;
    } // getInt
}
