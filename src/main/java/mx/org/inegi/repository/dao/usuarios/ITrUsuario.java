package mx.org.inegi.repository.dao.usuarios;

import java.util.List;
import mx.org.inegi.entity.dto.usuarios.TrUsuarioDto;

/**
 *
 * @author SALVADOR.GUTIERREZ
 */
public interface ITrUsuario {
    
    public List<TrUsuarioDto> getUsuarios();
    
    public Long addUsuario(TrUsuarioDto dto);
    
}
