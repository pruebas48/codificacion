package mx.org.inegi.repository.dao.usuarios;

import java.util.List;
import mx.org.inegi.entity.dto.usuarios.TrUsuarioDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author SALVADOR.GUTIERREZ
 */
@Repository
public class TrUsuarioDao implements ITrUsuario {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<TrUsuarioDto> getUsuarios() {
                String sql = """
                             select id_usuario,login from tr_usuario
                     """;
        return jdbcTemplate.query(sql, BeanPropertyRowMapper.newInstance(TrUsuarioDto.class));
    }

    @Override
    public Long addUsuario(TrUsuarioDto dto) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
