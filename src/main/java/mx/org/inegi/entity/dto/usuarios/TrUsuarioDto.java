package mx.org.inegi.entity.dto.usuarios;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author SALVADOR.GUTIERREZ
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity 
@Table(name = "TR_USUARIO")
public class TrUsuarioDto {

    @Id
    private Long idUsuario;
    private String login;
}
