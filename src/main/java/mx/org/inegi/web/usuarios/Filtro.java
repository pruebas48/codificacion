package mx.org.inegi.web.usuarios;

import jakarta.annotation.PostConstruct;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import lombok.Data;
import mx.org.inegi.entity.dto.usuarios.TrUsuarioDto;
import mx.org.inegi.service.usuarios.UsuarioServ;
import static mx.org.inegi.utility.Utility.*;
import org.primefaces.util.LangUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author SALVADOR.GUTIERREZ
 */
@Component
@Data
@Named("webUsuarios")
@ViewScoped
public class Filtro implements Serializable {

    @Autowired
    UsuarioServ serv;

    private List<TrUsuarioDto> list;
    private List<TrUsuarioDto> filteredList;

    @PostConstruct
    public void init() {
        loadData();
    }

    public void loadData() {
        this.list = serv.getUsuarios();
    }
    
     public boolean globalFilterList(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (LangUtils.isBlank(filterText)) {
            return true;
        }
        int filterInt = doInt(filterText);

        TrUsuarioDto usuarios = (TrUsuarioDto) value;
        return usuarios.getLogin().toLowerCase().contains(filterText)               
                || usuarios.getIdUsuario() == filterInt;
    }

}
