package mx.org.inegi.service.usuarios;

import java.util.List;
import lombok.RequiredArgsConstructor;
import mx.org.inegi.entity.dto.usuarios.TrUsuarioDto;
import mx.org.inegi.repository.dao.usuarios.ITrUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author SALVADOR.GUTIERREZ
 */
@Service
@RequiredArgsConstructor
public class UsuarioServ {

    @Autowired
    private ITrUsuario dao;

    public List<TrUsuarioDto> getUsuarios() {
        List<TrUsuarioDto> list;
        try {
            list = dao.getUsuarios();
        } catch (Exception e) {
            throw e;
        }
        return list;
    }
}
