FROM openjdk:17-jdk-slim
ARG JAR_FILE=target/codificacion.jar
COPY ${JAR_FILE} codificacion.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "codificacion.jar"]